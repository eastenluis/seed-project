import template from './header.html';

class HeaderController {
    constructor() {

    }
}

let HeaderComponent = {
    template,
    controller: HeaderController
};

export default HeaderComponent;