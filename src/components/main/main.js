import template from './main.html';
import './main.css';

class MainCtrl {
    constructor() {
        this.welcomeText = 'This is a seed project for AngularJS.';
    }

    $onInit() {

    }

    $onDestroy() {

    }
}

let MainComponent = {
    template: require('./main.html'),
    controller: MainCtrl
};

export default MainComponent;