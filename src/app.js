import angular from 'angular';
import 'angular-route';
import 'angular-ui-bootstrap';
import HeaderComponent from './components/header/header.js';
import MainComponent from './components/main/main.js';

let seedApp = angular.module('seedApp', ['ngRoute', 'ui.bootstrap']);

seedApp.config(($routeProvider) => {
    $routeProvider.when('/', { template: '<main></main>' });
});
seedApp.component('headerNav', HeaderComponent);
seedApp.component('main', MainComponent);