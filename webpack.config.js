module.exports = {
    entry: './src/app.js',
    output: {
        path: __dirname + "/build/",
        filename: "app.bundle.js"
    },
    module: {
        loaders: [
            { test: /\.js$/, exclude: /(node_modules|bower_components)/, loader: "babel-loader" },
            { test: /\.html$/, loader: "html-loader" },
            { test: /\.css$/, loader: ["style-loader", "css-loader"] }
        ]
    },
    devtool: "#source-map"
};
